package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Drzava;
import com.ftninformatika.jwd.test.model.Takmicar;
import com.ftninformatika.jwd.test.repository.DrzavaRepository;
import com.ftninformatika.jwd.test.service.DrzavaService;

@Service
public class JpaDrzavaService implements DrzavaService {
	
	@Autowired
	private DrzavaRepository drzavaRepository;

	@Override
	public Drzava findOneById(Long id) {
		return drzavaRepository.findOneById(id);
	}

	@Override
	public List<Drzava> findAll() {
		return drzavaRepository.findAll();
	}

	@Override
	public int getUkupanBrojMedalja(Drzava drzava) {
		int medalje = 0;
		for ( Takmicar takmicar : drzava.getTakmicari()) {
			medalje += takmicar.getBrojMedalja();
		}
		return medalje;
	}


}
