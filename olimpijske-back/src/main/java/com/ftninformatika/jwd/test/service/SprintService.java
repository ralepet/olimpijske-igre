package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Sprint;

public interface SprintService {
	
	Sprint findOneById(Long id);

	List<Sprint> findAll();
	
	Sprint save(Sprint sprint);
	
	public Sprint findOneByIme(String ime);
	
	int getUkupanBrojBodova(Sprint sprint);

}
