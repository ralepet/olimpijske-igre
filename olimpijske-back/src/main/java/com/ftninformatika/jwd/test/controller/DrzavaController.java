package com.ftninformatika.jwd.test.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.DrzavaDto;
import com.ftninformatika.jwd.test.dto.StatistikaDto;
import com.ftninformatika.jwd.test.model.Drzava;
import com.ftninformatika.jwd.test.service.DrzavaService;
import com.ftninformatika.jwd.test.support.DrzavaToDto;
import com.ftninformatika.jwd.test.support.DrzavaToStatistikaDto;
import com.ftninformatika.jwd.test.support.StatistikaDtoComparator;


@RestController
@RequestMapping(value = "/api/drzave", produces = MediaType.APPLICATION_JSON_VALUE)
public class DrzavaController {
	
	@Autowired
	private DrzavaService drzavaService;
	
	@Autowired
	private DrzavaToDto toDto;
	
	@Autowired
	private DrzavaToStatistikaDto toStatistikaDto;

	@GetMapping
	public ResponseEntity<List<DrzavaDto>> getAll() {

		List<Drzava> list = drzavaService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	@GetMapping("/statistika")
	public ResponseEntity<List<StatistikaDto>> getStatistika() {

		List<Drzava> list = drzavaService.findAll();
		
		//return new ResponseEntity<>(toStatistikaDto.convert(list), HttpStatus.OK); // ovo nek ostane, ukoliko ne zelimo na backendu da sortiramo
		
		// sad cemo sloziti listu stastika dto objekata
		List<StatistikaDto> stastistika = toStatistikaDto.convert(list); // prvo pretvorimo listu drzava u stastistikaDto
		
		Collections.sort(stastistika, new StatistikaDtoComparator(-1)); // zatim primenimo komparator, koji ce sortirati po broju medalja
			
		return new ResponseEntity<>(stastistika, HttpStatus.OK);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
