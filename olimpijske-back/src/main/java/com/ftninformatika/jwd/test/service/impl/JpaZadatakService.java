package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Sprint;
import com.ftninformatika.jwd.test.model.Zadatak;
import com.ftninformatika.jwd.test.repository.ZadatakRepository;
import com.ftninformatika.jwd.test.service.SprintService;
import com.ftninformatika.jwd.test.service.ZadatakService;

@Service
public class JpaZadatakService implements ZadatakService{
	
	@Autowired
	private ZadatakRepository zadatakRepository;
	
	@Autowired
	private SprintService sprintService;
	
	@Override
	public Zadatak findOneById(Long id) {
		return zadatakRepository.findOneById(id);
	}

	@Override
	public Zadatak save(Zadatak zadatak) {
		
		// DODATAK NA ZADATAK 1.3
		Sprint sprint = zadatak.getSprint();
		// parsiramo bodove iz sprinta jer su String, te dodajemo bodove iz novokreiranog zadatka
		int ukupnoBodova =  Integer.parseInt(sprint.getUkupnoBodova()) +  zadatak.getBodovi();
		// zatim setujemo bodove 
		sprint.setUkupnoBodova(Integer.toString(ukupnoBodova));
		// pozivamo servis koji ce menjati broj bodova
		sprintService.save(sprint);	
		
		return zadatakRepository.save(zadatak);
	}
	
	@Override
	public Zadatak update(Zadatak zadatak) {
		
		// DODATAK NA ZADATAK 1.3
		Sprint sprint = zadatak.getSprint();
		// broj bodova cemo dobiti tako sto cemo pozvaci sprintService
		int ukupnoBodova = sprintService.getUkupanBrojBodova(sprint);
		// zatim setujemo bodove
		sprint.setUkupnoBodova(Integer.toString(ukupnoBodova));
		// pozivamo servis koji ce menjati broj bodova
		sprintService.save(sprint);
		
		return zadatakRepository.save(zadatak);
	}
	
	@Override
	public void delete(Zadatak zadatak) { 
		// DODATAK NA ZADATAK 1.3
		Sprint sprint = zadatak.getSprint();
		// parsiramo bodove iz sprinta jer su String, te oduzimamo bodove iz
		// obrisanog zadatka
		int ukupnoBodova = Integer.parseInt(sprint.getUkupnoBodova()) - zadatak.getBodovi();
		// zatim setujemo bodove
		sprint.setUkupnoBodova(Integer.toString(ukupnoBodova));
		// pozivamo servis koji ce menjati broj bodova
		sprintService.save(sprint);
		
		zadatakRepository.deleteById(zadatak.getId());
		
	}
	
	// metoda koja ne oduzima bodove za sprint
	@Override
	public Zadatak delete(Long id) { 
		Optional<Zadatak> zadatak = zadatakRepository.findById(id);
		if (zadatak.isPresent()) {
			zadatakRepository.deleteById(id);
			return zadatak.get();
		}
		return null;
	}

	@Override
	public Page<Zadatak> findAll(Integer pageNo) {
		return zadatakRepository.findAll(PageRequest.of(pageNo, 4));
	}

	@Override
	public List<Zadatak> findAll() {
		return zadatakRepository.findAll();
	}

	@Override
	public Page<Zadatak> find(String imeZadatka, String imeSprinta, Integer pageNo) {
		
		if (imeZadatka == null) {
			imeZadatka = "";
        }
		
		Sprint sprint = sprintService.findOneByIme(imeSprinta);
		
		if (sprint != null) {
			return zadatakRepository.findByImeIgnoreCaseContainsAndSprintId(imeZadatka, sprint.getId(), PageRequest.of(pageNo, 4));
			
		}	
		return zadatakRepository.findByImeIgnoreCaseContains(imeZadatka,  PageRequest.of(pageNo, 4));
	}

}
