package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.TakmicarDto;
import com.ftninformatika.jwd.test.model.Takmicar;

@Component
public class TakmicarToDto implements Converter<Takmicar, TakmicarDto>{

	@Override
	public TakmicarDto convert(Takmicar takmicar) {
		// TODO Auto-generated method stub
		TakmicarDto dto = new TakmicarDto();
		dto.setId(takmicar.getId());
		dto.setBrojMedalja(takmicar.getBrojMedalja());
		dto.setDatumRodjenja(takmicar.getDatumRodjenja().toString());
		dto.setImePrezime(takmicar.getImePrezime());
		dto.setIdDrzave(takmicar.getDrzava().getId());
		// naziv i oznaka drzave su mi spojeni u Dto
		dto.setNazivOznakaDrzave(takmicar.getDrzava().getNaziv() + ", " + takmicar.getDrzava().getOznaka()); 
		return dto;
	}
	
	public List<TakmicarDto> convert(List<Takmicar>list){
		List<TakmicarDto>dto = new ArrayList<>();
		for(Takmicar takmicar : list) {   
			dto.add(convert(takmicar));
		}
		return dto;
	}
	
	

}
