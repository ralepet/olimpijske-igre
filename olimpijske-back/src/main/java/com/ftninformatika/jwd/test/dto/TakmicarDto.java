package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class TakmicarDto {
	
	private Long id;
	
	@NotNull(message = "Nije zadat br medalja.")
	@Positive(message = "Broj medalja mora biti pozitivan broj.")
	private int brojMedalja;
	
	@NotBlank(message = "Ime i prezime takmicara mora biti uneseno i ne sme biti prazan string.")
	@Size(max = 60, message = "Ime i prezime takmicara moze imati max 60 karaktera.")
	private String imePrezime;
	
	@Positive(message = "Id mora biti pozitivan broj.")
	@NotNull
	private Long idDrzave;
	
	private String nazivOznakaDrzave;
	
	private String datumRodjenja;

	public TakmicarDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBrojMedalja() {
		return brojMedalja;
	}

	public void setBrojMedalja(int brojMedalja) {
		this.brojMedalja = brojMedalja;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	
	public Long getIdDrzave() {
		return idDrzave;
	}

	public void setIdDrzave(Long idDrzave) {
		this.idDrzave = idDrzave;
	}


	public String getNazivOznakaDrzave() {
		return nazivOznakaDrzave;
	}

	public void setNazivOznakaDrzave(String nazivOznakaDrzave) {
		this.nazivOznakaDrzave = nazivOznakaDrzave;
	}

	public String getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}
	 

}
