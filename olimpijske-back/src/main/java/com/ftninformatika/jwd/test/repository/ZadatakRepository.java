package com.ftninformatika.jwd.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;

import com.ftninformatika.jwd.test.model.Zadatak;

@Repository
public interface ZadatakRepository extends JpaRepository<Zadatak, Long>{
	
	Zadatak findOneById(Long id);
	
	 Page<Zadatak> findByImeIgnoreCaseContainsAndSprintId( String Ime, Long SprintId, Pageable pageable);
	 
	 Page<Zadatak> findByImeIgnoreCaseContains( String Ime, Pageable pageable);

}
