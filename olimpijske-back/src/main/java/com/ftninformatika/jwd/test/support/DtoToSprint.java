package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.SprintDto;
import com.ftninformatika.jwd.test.model.Sprint;
import com.ftninformatika.jwd.test.service.SprintService;

@Component
public class DtoToSprint implements  Converter <SprintDto, Sprint>{
	
	@Autowired
	private SprintService sprintService;

	@Override
	public Sprint convert(SprintDto dto) {
		Sprint entity;   
		if (dto.getId() == null) {
			entity = new Sprint();
		} else {
			entity = sprintService.findOneById(dto.getId());
		}
		
		if(entity != null) {
			entity.setIme(dto.getIme());
			entity.setUkupnoBodova(dto.getUkupnoBodova());
		}

		return entity;
	}

}
