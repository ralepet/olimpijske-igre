package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class PrijavaDto {
	
	private Long id;
	
	@Positive(message = "Id mora biti pozitivan broj.")
	@NotNull
	private Long idTakmicara;
	
	private String datumPrijave;
	
	private String disciplina;

	public PrijavaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdTakmicara() {
		return idTakmicara;
	}

	public void setIdTakmicara(Long idTakmicara) {
		this.idTakmicara = idTakmicara;
	}

	public String getDatumPrijave() {
		return datumPrijave;
	}

	public void setDatumPrijave(String datumPrijave) {
		this.datumPrijave = datumPrijave;
	}

	public String getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(String disciplina) {
		this.disciplina = disciplina;
	}

	@Override
	public String toString() {
		return "PrijavaDto [id=" + id + ", idTakmicara=" + idTakmicara + ", datumPrijave=" + datumPrijave
				+ ", disciplina=" + disciplina + "]";
	}

}
