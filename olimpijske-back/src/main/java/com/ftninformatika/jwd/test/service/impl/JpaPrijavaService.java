package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.repository.PrijavaRepository;
import com.ftninformatika.jwd.test.service.PrijavaService;

@Service
public class JpaPrijavaService implements PrijavaService{
	
	@Autowired
	PrijavaRepository prijavaRepository;

	@Override
	public Prijava findOneById(Long id) {
		return prijavaRepository.findOneById(id);
	}
	@Override
	public List<Prijava> findAll() {
		return prijavaRepository.findAll();
	}

	@Override
	public Prijava save(Prijava prijava) {
		return prijavaRepository.save(prijava);
	}

	

}
