package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.DrzavaDto;
import com.ftninformatika.jwd.test.model.Drzava;
import com.ftninformatika.jwd.test.service.DrzavaService;

@Component
public class DtoToDrzava implements Converter<DrzavaDto, Drzava>{
	
	 @Autowired
	    private DrzavaService drzavaService;

	@Override
	public Drzava convert(DrzavaDto dto) {
		Drzava drzava = null;
		
		if (dto.getId() != null) {
			drzava = drzavaService.findOneById(dto.getId());
		}
		
		if(drzava == null) {
			drzava = new Drzava();
        }
		
		drzava.setNaziv(dto.getNaziv());
		drzava.setOznaka(dto.getOznaka());

		return drzava;
	}
	

}
