package com.ftninformatika.jwd.test.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Drzava {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true)
	private String naziv;
	
	@Column(nullable = false, unique = true, length = 3)
	private String oznaka;
	
	@OneToMany(mappedBy = "drzava", cascade = CascadeType.ALL)
	List<Takmicar> takmicari;

	public Drzava() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOznaka() {
		return oznaka;
	}

	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}

	public List<Takmicar> getTakmicari() {
		return takmicari;
	}

	public void setTakmicari(List<Takmicar> takmicari) {
		this.takmicari = takmicari;
	}
	
	

}
