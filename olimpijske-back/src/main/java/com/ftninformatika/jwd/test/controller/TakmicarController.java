package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.TakmicarDto;
import com.ftninformatika.jwd.test.model.Takmicar;
import com.ftninformatika.jwd.test.service.TakmicarService;
import com.ftninformatika.jwd.test.support.DtoToTakmicar;
import com.ftninformatika.jwd.test.support.TakmicarToDto;

@RestController
@RequestMapping(value = "/api/takmicari", produces = MediaType.APPLICATION_JSON_VALUE)
public class TakmicarController {
	
	@Autowired
	private TakmicarService takmicarService;

	@Autowired
	private TakmicarToDto toDto;
	
	@Autowired
	private DtoToTakmicar toTakmicar;
	
	/*
	@GetMapping
	public ResponseEntity<List<TakmicarDto>> getAll(
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/linije?pageNo=0 ili neki drugi broj umesto 0

		Page<Takmicar> page = takmicarService.findAll(pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), HttpStatus.OK);
	}
	*/
	
	@GetMapping
	public ResponseEntity<List<TakmicarDto>> getAll(
			@RequestParam(required=false) Integer brojMedaljaOd, 
			@RequestParam(required=false) Integer brojMedaljaDo,
			@RequestParam(required=false) Long drzava_id,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/linije?pageNo=0 ili neki drugi broj umesto 0
		
		//Page<Linija> page = linijaService.findAll(pageNo);

		Page<Takmicar> page = takmicarService.find(brojMedaljaOd, brojMedaljaDo, drzava_id, pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	 //@PreAuthorize("hasAnyAuthority('ADMIN', 'KORISNIK')")
	@GetMapping("/{id}")
    public ResponseEntity<TakmicarDto> getOne(@PathVariable Long id){
        Takmicar takmicar = takmicarService.findOneById(id);

        if(takmicar != null) {
            return new ResponseEntity<>(toDto.convert(takmicar), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
	
	// @PreAuthorize("hasAnyAuthority('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicarDto> create(@Valid @RequestBody TakmicarDto takmicarDto) {
		Takmicar takmicar = toTakmicar.convert(takmicarDto);

		if (takmicar.getDrzava() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Takmicar sacuvanTakmicar = takmicarService.save(takmicar);

		return new ResponseEntity<>(toDto.convert(sacuvanTakmicar), HttpStatus.CREATED);
	}
	
	// @PreAuthorize("hasAnyAuthority('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicarDto> update(@PathVariable Long id, @Valid @RequestBody TakmicarDto takmicarDto) {

		if (!id.equals(takmicarDto.getId())) { 
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Takmicar takmicar = toTakmicar.convert(takmicarDto);
		Takmicar sacuvanTakmicar = takmicarService.update(takmicar);

		return new ResponseEntity<>(toDto.convert(sacuvanTakmicar), HttpStatus.OK);
	}
	
	// @PreAuthorize("hasAnyAuthority('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Takmicar obrisanTakmicar = takmicarService.delete(id);

		if (obrisanTakmicar != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	

}
