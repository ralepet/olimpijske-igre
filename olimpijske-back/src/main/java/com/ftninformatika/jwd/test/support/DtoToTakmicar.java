package com.ftninformatika.jwd.test.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.TakmicarDto;
import com.ftninformatika.jwd.test.model.Drzava;
import com.ftninformatika.jwd.test.model.Takmicar;
import com.ftninformatika.jwd.test.service.DrzavaService;
import com.ftninformatika.jwd.test.service.TakmicarService;

@Component
public class DtoToTakmicar implements Converter<TakmicarDto, Takmicar>{
	

	@Autowired
	private DrzavaService drzavaService;

	@Autowired
	private TakmicarService takmicarService;

	@Override
	public Takmicar convert(TakmicarDto dto) {
		Takmicar takmicar = null;
		
		if (dto.getId() != null) {
			takmicar = takmicarService.findOneById(dto.getId());
		}
		
		if(takmicar == null) {
			takmicar = new Takmicar();
        }
		
		takmicar.setImePrezime(dto.getImePrezime());
		takmicar.setBrojMedalja(dto.getBrojMedalja());
		
		Drzava drzava = drzavaService.findOneById(dto.getIdDrzave());
		if(drzava != null) {
			takmicar.setDrzava(drzava);
		}
		takmicar.setDatumRodjenja(getLocalTime(dto.getDatumRodjenja()));
		
		
		return takmicar;
	} 
	
	private LocalDate getLocalTime(String date) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }

}
