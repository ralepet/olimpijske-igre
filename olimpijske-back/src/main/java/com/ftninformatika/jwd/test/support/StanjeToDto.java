package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.StanjeDto;
import com.ftninformatika.jwd.test.model.Stanje;

@Component
public class StanjeToDto implements Converter<Stanje, StanjeDto>{

	@Override
	public StanjeDto convert(Stanje source) {

		StanjeDto dto = new StanjeDto();
		dto.setId(source.getId());
		dto.setIme(source.getIme());
		return dto;
	}
	
	public List<StanjeDto> convert(List<Stanje>list){
		List<StanjeDto>dto = new ArrayList<StanjeDto>();
		for(Stanje stanje : list) {  
			dto.add(convert(stanje));
		}
		return dto;
	}

}
