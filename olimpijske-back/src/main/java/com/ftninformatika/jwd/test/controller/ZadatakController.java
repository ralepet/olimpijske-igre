package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.ZadatakDto;
import com.ftninformatika.jwd.test.model.Zadatak;
import com.ftninformatika.jwd.test.service.ZadatakService;
import com.ftninformatika.jwd.test.support.DtoToZadatak;
import com.ftninformatika.jwd.test.support.ZadatakToDto;

@RestController
@RequestMapping(value = "/api/zadaci", produces = MediaType.APPLICATION_JSON_VALUE)
public class ZadatakController {
	
	@Autowired
	private ZadatakService zadatakService;
	
	@Autowired
	private ZadatakToDto toDto;
	
	@Autowired
	private DtoToZadatak toZadatak;
	
	
//	@GetMapping
//	public ResponseEntity<List<ZadatakDto>> getAll(
//			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
//		
//		// putanja http://localhost:8080/api/zadaci?pageNo=0 ili neki drugi broj umesto 0
//
//		Page<Zadatak> page = zadatakService.findAll(pageNo);
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));
//
//		return new ResponseEntity<>(toDto.convert(page.getContent()), HttpStatus.OK);
//	}
	
	@GetMapping
	public ResponseEntity<List<ZadatakDto>> getAll(
			@RequestParam(required=false) String imeZadatka, 
			@RequestParam(required=false) String imeSprinta,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/zadaci?pageNo=0 ili neki drugi broj umesto 0
		
		//Page<Zadatak> page = zadatakService.findAll(pageNo);

		Page<Zadatak> page = zadatakService.find(imeZadatka, imeSprinta, pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
    public ResponseEntity<ZadatakDto> getOne(@PathVariable Long id){
		Zadatak zadatak = zadatakService.findOneById(id); 

        if(zadatak != null) {
            return new ResponseEntity<>(toDto.convert(zadatak), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
	
	//@PreAuthorize("hasAnyAuthority('ADMIN')") 
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ZadatakDto> create(@Valid @RequestBody ZadatakDto zadatakDto) {
		Zadatak zadatak = toZadatak.convert(zadatakDto);
 
		if (zadatak.getSprint() == null || zadatak.getStanje() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} 

		Zadatak sacuvaniZadatak = zadatakService.save(zadatak);

		return new ResponseEntity<>(toDto.convert(sacuvaniZadatak), HttpStatus.CREATED);
	}
	
	// @PreAuthorize("hasAnyAuthority('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ZadatakDto> update(@PathVariable Long id, @Valid @RequestBody ZadatakDto zadatakDto) {

		if (!id.equals(zadatakDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		

		Zadatak zadatak = toZadatak.convert(zadatakDto);

		// moze se preci samo u naredno stanje, poredi ih po id-u, ukoliko dto ima manji
		// id od entitiy-ja
		// to znaci da neko hoce da vrati u prethodno stanje i to cemo onemoguciti
		if (zadatakDto.getStanjeId() < zadatak.getStanje().getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Zadatak sacuvanZadatak = zadatakService.update(zadatak);

		return new ResponseEntity<>(toDto.convert(sacuvanZadatak), HttpStatus.OK);
	}
	
//	// @PreAuthorize("hasAnyAuthority('ADMIN')")
//		@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
//		public ResponseEntity<ZadatakDto> update(@PathVariable Long id, @Valid @RequestBody ZadatakDto zadatakDto) {
//
//			if (!id.equals(zadatakDto.getId())) {
//				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//			}
//
//			Zadatak zadatak = toZadatak.convert(zadatakDto);
//
//			// moze se preci samo u naredno stanje, poredi ih po id-u, ukoliko dto ima manji
//			// id od entitiy-ja
//			// to znaci da neko hoce da vrati u prethodno stanje i to cemo onemoguciti
//			if (zadatakDto.getStanjeId() < zadatak.getStanje().getId()) {
//				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//			}
//
//			Zadatak sacuvanZadatak = zadatakService.update(zadatak);
//
//			return new ResponseEntity<>(toDto.convert(sacuvanZadatak), HttpStatus.OK);
//		}
	
	// @PreAuthorize("hasAnyAuthority('ADMIN')")
		@DeleteMapping("/{id}")
		public ResponseEntity<Void> delete(@PathVariable Long id) {
			Zadatak obrisanZadatak = zadatakService.findOneById(id);

			if (obrisanZadatak != null) {
				zadatakService.delete(obrisanZadatak);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		}
		
//	// @PreAuthorize("hasAnyAuthority('ADMIN')")
//	@DeleteMapping("/{id}")
//	public ResponseEntity<Void> delete(@PathVariable Long id) {
//		Zadatak obrisanZadatak = zadatakService.delete(id);
//
//		if (obrisanZadatak != null) {
//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//		} else {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//	}

}
