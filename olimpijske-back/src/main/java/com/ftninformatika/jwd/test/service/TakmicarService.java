package com.ftninformatika.jwd.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.model.Takmicar;

public interface TakmicarService {
	
	Takmicar findOneById(Long id);

	Takmicar save(Takmicar takmicar);

	Takmicar update(Takmicar takmicar);

	Takmicar delete(Long id);

	Page<Takmicar> findAll(Integer pageNo);

	List<Takmicar> findAll();
	
	Page<Takmicar> find(Integer brojMedaljaOd,  Integer brojMedaljaDo, Long drzava_id, Integer pageNo);

}
