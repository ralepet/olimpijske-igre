package com.ftninformatika.jwd.test.dto;

public class StatistikaDto {
	
	private Long id;
	
	private String nazivOznakaDrzave;
	
	private Integer brojMedalja;

	public StatistikaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivOznakaDrzave() {
		return nazivOznakaDrzave;
	}

	public void setNazivOznakaDrzave(String nazivOznakaDrzave) {
		this.nazivOznakaDrzave = nazivOznakaDrzave;
	}

	public Integer getBrojMedalja() {
		return brojMedalja;
	}

	public void setBrojMedalja(Integer brojMedalja) {
		this.brojMedalja = brojMedalja;
	}

	@Override
	public String toString() {
		return "StatistikaDto [id=" + id + ", nazivOznakaDrzave=" + nazivOznakaDrzave + ", brojMedalja=" + brojMedalja
				+ "]";
	}

}
