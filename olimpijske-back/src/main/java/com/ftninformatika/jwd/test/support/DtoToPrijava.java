package com.ftninformatika.jwd.test.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PrijavaDto;
import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.service.PrijavaService;
import com.ftninformatika.jwd.test.service.TakmicarService;

@Component
public class DtoToPrijava implements Converter<PrijavaDto, Prijava>{
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private TakmicarService takmicarService;

	@Override
	public Prijava convert(PrijavaDto dto) {
		Prijava prijava = null;
		
		if (dto.getId() != null) {
			prijava = prijavaService.findOneById(dto.getId());
		}
		
		if(prijava == null) {
			prijava = new Prijava();
        }
		
		prijava.setTakmicar(takmicarService.findOneById(dto.getIdTakmicara()));
		//prijava.setDatumPrijave(getLocalTime(dto.getDatumPrijave()));
		prijava.setDisciplina(dto.getDisciplina());
		
		return prijava;
	}
	
	/*
	private LocalDate getLocalTime(String date) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }
    */

}
