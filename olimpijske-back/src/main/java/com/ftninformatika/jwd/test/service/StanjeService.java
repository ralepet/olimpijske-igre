package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Stanje;

public interface StanjeService {
	
	Stanje findOneById(Long id);

	List<Stanje> findAll();
	
	Stanje save(Stanje stanje);

}
