package com.ftninformatika.jwd.test.support;

import java.util.Comparator;

import com.ftninformatika.jwd.test.dto.StatistikaDto;

public class StatistikaDtoComparator implements Comparator<StatistikaDto>{
	
	int direction = 1;

	public StatistikaDtoComparator(int direction) {
		if (direction != 1 && direction != -1) {
			direction = 1;
		}
		this.direction = direction;
	}

	@Override
	public int compare(StatistikaDto o1, StatistikaDto o2) {
		int retVal = 0;
		if(o1!= null && o2!=null){	
			retVal = (int) (o1.getBrojMedalja() - o2.getBrojMedalja());
		}
		return retVal * direction;
	}

}
