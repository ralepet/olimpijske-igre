package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.StanjeDto;
import com.ftninformatika.jwd.test.model.Stanje;
import com.ftninformatika.jwd.test.service.StanjeService;

@Component
public class DtoToStanje implements Converter <StanjeDto, Stanje>{
	
	@Autowired
	private StanjeService stanjeService;

	@Override
	public Stanje convert(StanjeDto dto) {
		Stanje entity;   
		if (dto.getId() == null) {
			entity = new Stanje();
		} else {
			entity = stanjeService.findOneById(dto.getId());
		}
		
		if(entity != null) {
			entity.setIme(dto.getIme());
		}

		return entity;
	}

}
