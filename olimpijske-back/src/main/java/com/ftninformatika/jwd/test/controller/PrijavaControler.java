package com.ftninformatika.jwd.test.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.PrijavaDto;
import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.service.PrijavaService;
import com.ftninformatika.jwd.test.support.DtoToPrijava;
import com.ftninformatika.jwd.test.support.PrijavaToDto;

@RestController
@RequestMapping(value = "/api/prijave", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrijavaControler {
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private PrijavaToDto toDto;
	
	@Autowired
	private DtoToPrijava toPrijava;
	
	@GetMapping
	public ResponseEntity<List<PrijavaDto>> getAll() {

		List<Prijava> list = prijavaService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	// @PreAuthorize("hasAnyAuthority('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrijavaDto> create(@Valid @RequestBody PrijavaDto prijavaDto) { 
		Prijava prijava = toPrijava.convert(prijavaDto);
		prijava.setDatumPrijave(LocalDate.now());

		if (prijava.getTakmicar() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Prijava sacuvanaPrijava = prijavaService.save(prijava);

		return new ResponseEntity<>(toDto.convert(sacuvanaPrijava), HttpStatus.CREATED);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
	
	

}
