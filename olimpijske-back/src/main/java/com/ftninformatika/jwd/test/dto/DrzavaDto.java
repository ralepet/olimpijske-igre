package com.ftninformatika.jwd.test.dto;

public class DrzavaDto {
	
	 private Long id;
	 
	 private String naziv;
	 
	 private String oznaka;

	
	public DrzavaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOznaka() {
		return oznaka;
	}

	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}
	 
	 

}
