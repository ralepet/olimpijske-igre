package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.ZadatakDto;
import com.ftninformatika.jwd.test.model.Zadatak;
import com.ftninformatika.jwd.test.service.SprintService;
import com.ftninformatika.jwd.test.service.StanjeService;
import com.ftninformatika.jwd.test.service.ZadatakService;

@Component
public class DtoToZadatak implements Converter <ZadatakDto, Zadatak>{
	
	@Autowired
	private ZadatakService zadatakService;
	
	@Autowired
	private SprintService sprintService;
	
	@Autowired
	private StanjeService stanjeService;

	@Override
	public Zadatak convert(ZadatakDto dto) {
		Zadatak entity;   
		if (dto.getId() == null) {
			entity = new Zadatak();
		} else {
			entity = zadatakService.findOneById(dto.getId());
		}
		
		if(entity != null) {
			entity.setIme(dto.getIme());
			entity.setBodovi(dto.getBodovi());
			entity.setSprint(sprintService.findOneById(dto.getSprintId()));
			entity.setStanje(stanjeService.findOneById(dto.getStanjeId()));
			entity.setZaduzeni(dto.getZaduzeni());
		}	
		
		return entity;
	}

}
