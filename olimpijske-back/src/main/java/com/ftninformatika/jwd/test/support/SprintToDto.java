package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.SprintDto;
import com.ftninformatika.jwd.test.model.Sprint;


@Component
public class SprintToDto implements Converter<Sprint, SprintDto>{

	@Override
	public SprintDto convert(Sprint source) {
		SprintDto dto = new SprintDto();
		dto.setId(source.getId());
		dto.setIme(source.getIme());
		dto.setUkupnoBodova(source.getUkupnoBodova());
		return dto;
	}
	
	public List<SprintDto> convert(List<Sprint>list){
		List<SprintDto>dto = new ArrayList<SprintDto>();
		for(Sprint sprint : list) {  
			dto.add(convert(sprint));
		}
		return dto;
	}

}
