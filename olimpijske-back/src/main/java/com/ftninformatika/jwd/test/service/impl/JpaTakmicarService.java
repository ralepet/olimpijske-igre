package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Takmicar;
import com.ftninformatika.jwd.test.repository.TakmicarRepository;
import com.ftninformatika.jwd.test.service.TakmicarService;


@Service
public class JpaTakmicarService implements TakmicarService{
	
	@Autowired
	private TakmicarRepository takmicarRepository;

	@Override
	public Takmicar findOneById(Long id) {
		return takmicarRepository.findOneById(id);
	}

	@Override
	public Takmicar save(Takmicar takmicar) {
		takmicar.getDrzava().getTakmicari().add(takmicar); // nema potrebe da se zove servis
		return takmicarRepository.save(takmicar);
	}

	@Override
	public Takmicar update(Takmicar takmicar) {
		return takmicarRepository.save(takmicar);
	}

	@Override
	public Takmicar delete(Long id) { 
		Optional<Takmicar> takmicar = takmicarRepository.findById(id);
		if (takmicar.isPresent()) {
			takmicarRepository.deleteById(id);
			return takmicar.get();
		}
		return null;
	}

	@Override
	public Page<Takmicar> findAll(Integer pageNo) {
		return takmicarRepository.findAll(PageRequest.of(pageNo, 5));
	}

	@Override
	public List<Takmicar> findAll() {
		return takmicarRepository.findAll();
	}

	@Override
	public Page<Takmicar> find(Integer brojMedaljaOd, Integer brojMedaljaDo, Long drzava_id, Integer pageNo) {
		
		if (brojMedaljaOd == null) {
			brojMedaljaOd = 0;
        }
		
		if (brojMedaljaDo == null) {
			brojMedaljaDo = Integer.MAX_VALUE;
        }
		

        if (drzava_id == null) {
        	return takmicarRepository.findByBrojMedaljaBetween(
        			brojMedaljaOd, brojMedaljaDo, PageRequest.of(pageNo, 4));
        }
        
		return takmicarRepository.findByBrojMedaljaBetweenAndDrzavaId(brojMedaljaOd, brojMedaljaDo, drzava_id,  PageRequest.of(pageNo, 4));
	}


}
