package com.ftninformatika.jwd.test.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Takmicar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true)
	private String imePrezime;
	
	@Column
	private int brojMedalja;
	
	@Column(nullable = false)
	private LocalDate datumRodjenja;
	
	@ManyToOne
	private Drzava drzava;
	
	@OneToMany(mappedBy = "takmicar", cascade = CascadeType.ALL)
	List<Prijava> prijave;

	public Takmicar() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public int getBrojMedalja() {
		return brojMedalja;
	}

	public void setBrojMedalja(int brojMedalja) {
		this.brojMedalja = brojMedalja;
	}

	public LocalDate getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(LocalDate datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public Drzava getDrzava() {
		return drzava;
	}

	public void setDrzava(Drzava drzava) {
		this.drzava = drzava;
	}
	
	
	public List<Prijava> getPrijave() {
		return prijave;
	}

	public void setPrijave(List<Prijava> prijave) {
		this.prijave = prijave;
	}

	@Override
	public String toString() {
		return "Takmicar [id=" + id + ", imePrezime=" + imePrezime + ", brojMedalja=" + brojMedalja + ", datumRodjenja="
				+ datumRodjenja + ", drzava=" + drzava.getNaziv() + "]";
	}
	
	

}
