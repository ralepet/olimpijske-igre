package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.DrzavaDto;
import com.ftninformatika.jwd.test.model.Drzava;

@Component
public class DrzavaToDto implements  Converter<Drzava, DrzavaDto>{

	@Override
	public DrzavaDto convert(Drzava drzava) {
		DrzavaDto dto = new DrzavaDto();
		dto.setId(drzava.getId());
		dto.setNaziv(drzava.getNaziv());
		dto.setOznaka(drzava.getOznaka());
		return dto;
	}
	
	public List<DrzavaDto> convert(List<Drzava>list){
		List<DrzavaDto>dto = new ArrayList<>();
		for(Drzava drzava : list) {  
			dto.add(convert(drzava));
		}
		return dto;
	}

}
