package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Drzava;

public interface DrzavaService {
	
	Drzava findOneById(Long id);

	List<Drzava> findAll();
	
	//Drzava save(Drzava drzava);
	
	int getUkupanBrojMedalja(Drzava drzava);

}
