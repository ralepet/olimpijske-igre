package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.StatistikaDto;
import com.ftninformatika.jwd.test.model.Drzava;
import com.ftninformatika.jwd.test.service.DrzavaService;

@Component
public class DrzavaToStatistikaDto implements Converter<Drzava, StatistikaDto>{
	
	@Autowired
	private DrzavaService drzavaService;

	@Override
	public StatistikaDto convert(Drzava drzava) {
		StatistikaDto dto = new StatistikaDto();
		dto.setId(drzava.getId());
		dto.setNazivOznakaDrzave(drzava.getNaziv() + ", " + drzava.getOznaka());
		dto.setBrojMedalja(drzavaService.getUkupanBrojMedalja(drzava));
		return dto;
	}
	
	public List<StatistikaDto> convert(List<Drzava>list){
		List<StatistikaDto>dto = new ArrayList<>();
		for(Drzava drzava : list) {     
			dto.add(convert(drzava));
		}
		return dto;
	}

}
