INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');
              
              /*
               * sifra za marko markovic je 1234
               */

INSERT INTO drzava (id, naziv, oznaka) VALUES (1,'Serbia', 'SRB');
INSERT INTO drzava (id, naziv, oznaka) VALUES (2,'Jamaica', 'JAM');
INSERT INTO drzava (id, naziv, oznaka) VALUES (3,'United States of America', 'USA');
INSERT INTO drzava (id, naziv, oznaka) VALUES (4,'Russia', 'RUS');

INSERT INTO takmicar (id, broj_medalja, datum_rodjenja, ime_prezime, drzava_id) VALUES (1, 3, '1987-12-12', 'Novak Djokovic', 1);
INSERT INTO takmicar (id, broj_medalja, datum_rodjenja, ime_prezime, drzava_id) VALUES (2, 5, '1985-02-15', 'Uain Bolt', 2);
INSERT INTO takmicar (id, broj_medalja, datum_rodjenja, ime_prezime, drzava_id) VALUES (3, 8, '1986-03-16', 'Michael Phelps', 3);
INSERT INTO takmicar (id, broj_medalja, datum_rodjenja, ime_prezime, drzava_id) VALUES (4, 2, '1988-04-16', 'Jasna Sekaric', 1);
INSERT INTO takmicar (id, broj_medalja, datum_rodjenja, ime_prezime, drzava_id) VALUES (5, 3, '1990-05-18', 'Maria Lasitskene', 4);

INSERT INTO prijava (id, datum_prijave, disciplina, takmicar_id) VALUES (1, '2022-03-16', 'Tenis', 1);
INSERT INTO prijava (id, datum_prijave, disciplina, takmicar_id) VALUES (2, '2022-03-16', 'Sprint 100m', 2);
INSERT INTO prijava (id, datum_prijave, disciplina, takmicar_id) VALUES (3, '2022-03-16', 'Sprint 200m', 2);

              


