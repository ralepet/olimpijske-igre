import React from 'react';
import TestAxios from './../../apis/TestAxios';
import { Row, Col, Table } from 'react-bootstrap'
import './../../index.css';
import {withNavigation } from '../../routeconf'

class Statistika extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            drzaveStatistika: []
        }
    }

    componentDidMount() {
        this.getDrzave();
    }

    async getDrzave(){
        try{
            let result = await TestAxios.get("/drzave/statistika");
            let drzaveStatistika = result.data; 
            this.setState({drzaveStatistika: drzaveStatistika});
            console.log("Statistika je uspesno dobavljena");
        }catch(error){
            console.log(error);
            alert("Statistika nije dobavljena");
        } 
    }

    renderStatistike() {
        return this.state.drzaveStatistika.map((stastistikaDrzave, index) => {
            return (
                <tr key={stastistikaDrzave.id}>
                    <td>{stastistikaDrzave.nazivOznakaDrzave}</td>
                    <td>{stastistikaDrzave.brojMedalja}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <Col>
                <Row>
                    <Table className="table table-striped" style={{ marginTop: 5 }} >
                        <thead className="thead-dark">
                            <tr>
                                <th>Drzava</th>
                                <th>Boj osvojenih medalja</th> 
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderStatistike()}
                        </tbody>
                    </Table>
                </Row>
            </Col>
        );
    }
}

export default withNavigation(Statistika);