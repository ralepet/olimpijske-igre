import React from 'react';
import ReactDOM from 'react-dom';
import { Navigate, Route, Link, HashRouter as Router, Routes } from 'react-router-dom';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';
import Home from './components/Home';
import Login from './components/authorization/Login'

import Takmicari from './components/takmicari/Takmicari';
import KreirajTakmicara from './components/takmicari/KreirajTakmicara'; 
import Statistika from './components/drzave/Statistika'; 

import NotFound from './components/NotFound';
import { logout } from './services/auth';
import Prijava from './components/takmicari/Prijava';


class App extends React.Component {

    render() {
        const jwt = window.localStorage.getItem("jwt")
        if(jwt){
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/takmicari">
                                    Takmicari
                                </Nav.Link>
                            </Nav>
                            <Button onClick={() => logout()}>Logout</Button>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/takmicari" element={<Takmicari />} />
                                {window.localStorage.getItem("role") === "ROLE_KORISNIK" ? <Route path="/takmicari/prijava/:id" element={<Prijava />} /> 
                                : <Route path="/takmicari/prijava/:id" element={<Navigate replace to="/takmicari" />} /> }
                                {window.localStorage.getItem("role") === "ROLE_ADMIN" ? <Route path="/takmicari/kreiraj" element={<KreirajTakmicara />} /> 
                                : <Route path="/takmicari/kreiraj" element={<Navigate replace to="/takmicari" />} /> }
                                <Route path="/statistika" element={<Statistika />} />
                                <Route path="/login" element={<Navigate replace to="/takmicari" />} />
                                <Route path="*" element={<NotFound />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }else{
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/login">
                                    Login
                                </Nav.Link>
                            </Nav>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/login" element={<Login />} />
                                <Route path="*" element={<Navigate replace to="/login" />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }
        
    }
};


ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
